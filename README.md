
# Ghost Invasion

Pac-Man clone written for Turbo Pascal by George M. Tzoumas, ported to Free Pascal by [RetroNick2020](https://github.com/RetroNick2020) and [rchastain](https://gitlab.com/rchastain).

## Links

* [MSPACMAN](http://cgi.di.uoa.gr/~geotz/dos/)
* [Discussion](https://forum.lazarus.freepascal.org/index.php/topic,54531.0.html)

## Screenshot

![Screenshot](screenshot.png)
